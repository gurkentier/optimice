/**
 * Copyright 2020 by gurkentier
 * Optimice
 * Keep your objects small like mice!
 */
(function (global) {

    let Optimice = function (data) {
        return new Optimice.init(data);
    };

    Array.prototype.unique = function () {
        return Array.from(new Set(this.map(JSON.stringify))).map(JSON.parse);
    };

    Array.prototype.indexOfArray = function (arr) {
        return this.map(JSON.stringify).indexOf(JSON.stringify(arr));
    };

    function encodeObject(obj, keys, prefix, suffix) {
        const templates = buildTemplateForObject(obj).unique();
        const values = compressObject(obj, templates, prefix, suffix);
        return {
            templates: templates,
            values: values,
            prefix: prefix,
            suffix: suffix,
        };
    }

    function compressObject(obj, templates, prefix, suffix) {
        const keys = Object.keys(obj);
        let value = keys.map((key) => {
            return Array.isArray(obj[key])
                ? compressObjectCollection(obj[key], templates, prefix, suffix)
                : obj[key]
        });
        value.push(prefix + getTemplateForObject(obj, templates) + suffix);
        return value;
    }

    function compressObjectCollection(arr, templates, prefix, suffix) {
        if (Array.isArray(arr)) {
            return arr.map((el) => {
                return arr.every(el => !(el instanceof Array) && (el instanceof Object))
                    ? compressObject(el, templates, prefix, suffix)
                    : el
            })
        }
        return null;
    }

    function processArray(arr, prefix, suffix, templates) {
        const template = getTemplateForArray(arr, prefix, suffix, templates);
        let result = {};
        for (let i = 0; i < arr.length; i++) {
            let item = arr[i];
            const itemIsArray = Array.isArray(item);
            const itemIsNestedArray = (itemIsArray && item.every(el => Array.isArray(el)));
            const itemIsArrayWithIdentifier = itemIsArray && (typeof getLastElement(item) === 'string') && getLastElement(item).indexOf(prefix) > -1;
            const key = template[i];
            if (!key) continue;
            result[key] = itemIsArrayWithIdentifier
                ? processArray(item, prefix, suffix, templates)
                : itemIsNestedArray ? processNestedArray(item, prefix, suffix, templates) : item;
        }
        return result;
    }

    function processNestedArray(nested, prefix, suffix, templates) {
        return nested.map((arr) => {
            return processArray(arr, prefix, suffix, templates);
        });
    }

    function getTemplateForObject(obj, templates) {
        let template = Object.keys(obj);
        return templates.indexOfArray(template);
    }

    function buildTemplateForObject(obj) {
        let result = [];
        let keys = Object.keys(obj);
        let template = [];
        let isArray = false;
        keys.forEach((key) => {
            template.push(key);
            if (Array.isArray(obj[key])) {
                isArray = true;
                buildTemplatesFromArray(obj[key]).forEach(t => {
                    t.forEach(i => result.push(i));
                });
            } else {
                isArray = false;
            }
        });
        result.push(template);
        return result;
    }


    function buildTemplatesFromArray(arr) {
        let template = [];
        if (Array.isArray(arr)) {
            if (arr.some((el) => {
                return !(el instanceof Array) && (el instanceof Object)
            })) {
                arr.forEach((el) => {
                    template.push(buildTemplateForObject(el));
                })
            }
        }

        return template;
    }

    function getTemplateForArray(arr, prefix, suffix, templates) {
        let key = getNumericIndexFromIdentifier(getLastElement(arr), prefix, suffix);
        return (templates)[key];
    }

    function getNumericIndexFromIdentifier(identifier, prefix, suffix) {
        return identifier.replace(prefix, '').replace(suffix, '');
    }

    function getLastElement(arr) {
        return arr.slice(-1)[0];
    }

    function getPrefix() {
        return this.hasOwnProperty('prefix') ? this.prefix : 'I:';
    }

    function getSuffix() {
        return this.hasOwnProperty('suffix') ? this.suffix : '';
    }

    function getTemplates() {
        return this.hasOwnProperty('templates') ? this.templates : [];
    }

    function getValuesToProcess() {
        return this.hasOwnProperty('values') ? this.values : [];
    }


    Optimice.prototype = {
        decode: function () {
            return processArray(this.data, this.prefix, this.suffix, this.templates);
        },
        encode: function () {
            return encodeObject(this.obj, Object.keys(this.obj), this.prefix, this.suffix);
        },
        unwrap: function () {
            const decoded = this.decode();
            const containerKey = (Object.keys(decoded)).length === 1 ? Object.keys(decoded)[0] : null;
            if (!containerKey) return false;
            return containerKey ? decoded[containerKey] : false;
        },
        wrap: function () {
            const container = {container: this.obj};
            return encodeObject(container, Object.keys(container), this.prefix, this.suffix);
        }
    };

    Optimice.init = function (data) {
        let self = this;
        self.obj = data;
        self.prefix = getPrefix.call(data);
        self.suffix = getSuffix.call(data);
        self.templates = getTemplates.call(data);
        self.data = getValuesToProcess.call(data);
    };

    /**
     * Expose prototype to global object
     */
    Optimice.init.prototype = Optimice.prototype;
    global.Optimice = global.o_ = Optimice;

})(typeof window !== "undefined" ? window : typeof global !== undefined  ? global : Function('return this')() || (42, eval)('this'));

/**
let data = {container: []};
for (let i = 0; i < 1; i++) {
    data.container.push(
        {
            "Lorem": "Ipsum",
            "Dolor": 223,
            "Hello": []
        },
        {
            "id": 1020122,
            "someImportantNumber": 1337,
            "anotherImportantNumber": 42,
            "created_at": "1990-12-09T09:40:30.000Z",
            "updated_at": "2020-01-10T10:37:45.000Z",
            "status": "new",
            "reference_id": 4321,
            "someArrayContainingObjects": [
                {
                    "id": 12,
                    "name": "Object 1"
                },
                {
                    "id": 34,
                    "name": "Object 2"
                },
                {
                    "id": 56,
                    "name": "Object 3"
                },
                [1, 2, 3]
            ],
            "completed": false,
            "is_new": false,
            "is_cancelled": false,
            "some_numbers": [12, 14, 31],
            "object": {
                "date": "2019-11-05",
                "date2": "2020-11-06",
                "nestedObject": {
                    "id": 3748234,
                    "message": "optimice me!",
                    "key": "a00ae0f16c288e60b"
                }
            }
        }
    );
}


function prettyPrint(msg, content) {
    console.log('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
    console.log('>>>>>>>>>>>>>>>>> ' + msg);
    console.log('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
    console.log(content, '\n');
}

prettyPrint('data before compression: ', data.container);
let encoded = o_(data.container).wrap();
let decoded = o_(encoded).unwrap();
//prettyPrint('data after compression: ', encoded);
prettyPrint('data after decompression: ', decoded);

console.log(decoded[1].someArrayContainingObjects);
//console.log(decoded[1].object);
console.log(encoded.templates);
/**
 * @TODO
 * nested object
 * array with mixed types (except primitives) - working partially now
 * arrays of object must only contain objects - solved
 */
