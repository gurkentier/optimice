# **optimice**  
### *Keep your objects small like mice!*  
optimice is a small library which allows you to optimise your (especially large) JSON objects by removing key redundancy.  
Maybe I will optimise optimice in the future, when I find some time.  
Feel free to contribute, fork, use, distribute (as long as you credit the author).  
  
#### **Usage**  
  
##### Encoding
To encode some data, just do the following:  
```javascript
let largeArrayWithObjects = 
[
    {  
        "id": 1001001,  
        "someImportantNumber": 1337,  
        "anotherImportantNumber": 42,  
        "created_at": "1990-12-09T09:40:30.000Z",  
        "updated_at": "2020-01-10T10:37:45.000Z",  
        "status": "new",  
        "reference_id": 4321,  
        "someArrayContainingObjects": [  
            {  
                "id": 12,  
                "name": "Object 1"  
            },  
            {  
                "id": 34,  
                "name": "Object 2"  
            },  
            {  
                "id": 56,  
                "name": "Object 3"  
            }  
        ],  
        "completed": false,  
        "is_new": false,  
        "is_cancelled": false,  
        "some_numbers": [12, 14, 31],  
        "object": {  
            "date": "2019-11-05",  
            "date2": "2020-11-06",  
            "nestedObject": {  
                "id": 3748234,  
                "message": "optimice me!",  
                "key": "a00ae0f16c288e60b"  
            }  
        }  
    },
    {  
        "id": 1020122,  
        "someImportantNumber": 1337,  
        "anotherImportantNumber": 42,  
        "created_at": "1990-12-09T09:40:30.000Z",  
        "updated_at": "2020-01-10T10:37:45.000Z",  
        "status": "new",  
        "reference_id": 4321,  
        "someArrayContainingObjects": [  
            {  
                "id": 12,  
                "name": "Object 1"  
            },  
            {  
                "id": 34,  
                "name": "Object 2"  
            },  
            {  
                "id": 56,  
                "name": "Object 3"  
            }  
        ],  
        "completed": false,  
        "is_new": false,  
        "is_cancelled": false,  
        "some_numbers": [12, 14, 31],  
        "object": {  
            "date": "2019-11-05",  
            "date2": "2020-11-06",  
            "nestedObject": {  
                "id": 3748234,  
                "message": "optimice me!",  
                "key": "a00ae0f16c288e60b"  
            }  
        }  
    }
]

let data = { container: largeArrayWithObjects };
const encoded = o_(data).encode;
console.log(encoded);
```  
Put your array containing objects into a new object (in this case "data") and assign it to some property (I recommend using container, but could be anything!)  
Just make sure not using a reserved property name like *obj, prefix, suffix, templates, data* since they are used by the library!  
Now you should have successfully encoded your objects!  
  
##### Decoding  

```javascript
let encodedObject = 
{
    "templates":[["id","name"],["id","someImportantNumber","anotherImportantNumber","created_at","updated_at","status","reference_id","someArrayContainingObjects","completed","is_new","is_cancelled","some_numbers","object"],["container"]],
    "values":[[[1001001,1337,42,"1990-12-09T09:40:30.000Z","2020-01-10T10:37:45.000Z","new",4321,[[12,"Object 1","I:0"],[34,"Object 2","I:0"],[56,"Object 3","I:0"]],false,false,false,[12,14,31],{"date":"2019-11-05","date2":"2020-11-06","nestedObject":{"id":3748234,"message":"optimice me!","key":"a00ae0f16c288e60b"}},"I:1"],[1020122,1337,42,"1990-12-09T09:40:30.000Z","2020-01-10T10:37:45.000Z","new",4321,[[12,"Object 1","I:0"],[34,"Object 2","I:0"],[56,"Object 3","I:0"]],false,false,false,[12,14,31],{"date":"2019-11-05","date2":"2020-11-06","nestedObject":{"id":3748234,"message":"optimice me!","key":"a00ae0f16c288e60b"}},"I:1"]],"I:2"],
    "prefix":"I:",
    "suffix":""
};


const decoded = o_(encodedObject).decode();
console.log(decoded);
``` 
Now you have succesfully decoded your objects and they are human readable again!  
